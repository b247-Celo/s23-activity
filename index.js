/*
	1. In the s23 folder, create an activity folder and an index.html and script.js file inside of it.
	2. Link the script.js file to the index.html file.
	3. Create a trainer object using object literals.
	4. Initialize/add the following trainer object properties:
	Name (String)
	Age (Number)
	Pokemon (Array)
	Friends (Object with Array values for properties)
	5. Initialize/add the trainer object method named talk that prints out the message Pikachu! I choose you!
	6. Access the trainer object properties using dot and square bracket notation.
	7. Invoke/call the trainer talk object method.
	8. Create a constructor for creating a pokemon with the following properties:
	Name (Provided as an argument to the contructor)
	Level (Provided as an argument to the contructor)
	Health (Create an equation that uses the level property)
	Attack (Create an equation that uses the level property)
	9. Create/instantiate several pokemon object from the constructor with varying name and level properties.
	10. Create a tackle method that will subtract the health property of the target pokemon object with the attack property of the object that used the tackle method.
	11. Create a faint method that will print out a message of targetPokemon has fainted.
	12. Create a condition in the tackle method that if the health property of the target pokemon object is less than or equal to 0 will invoke the faint method.
	13. Invoke the tackle method of one pokemon object to see if it works as intended.
	14. Create a git repository named s23.
	15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
	16. Add the link in Boodle.
*/

	let trainer = {
	    name: "Ash Ketchum",
	    age: 10,
	    Pokemon: ["Pikachu", "Charlizard", "Squirtle", "Bulbasaur"],
	    friends: {
	        hoenn: ["May", "Max"],
	        kanto: ["Brock", "Misty"]
	    },
	    talk: function() {
	        console.log("Pikachu! I choose you!");
	    }
	}

	console.log(trainer);
	console.log('Result of dot notation:');
	console.log(trainer.name);
	console.log('Result of square bracket notation:');
	console.log(trainer['pokemon']);
	console.log('Result of talk method');
	trainer.talk();

	

	function Pokemon(name, level){
	    this.name = name;
	    this.level = level;
	    this.health = 2 * level;
	    this.attack = level;

	    this.tackle = function(target){
	        let healthLeft = target.health - this.attack
	        console.log(this.name + ' tackled ' + target.name);
	        console.log(target.name + " health is now reduced to " + (healthLeft));
	        
	       if (healthLeft <= 0) {
	        target.faint();
	       }

	        return target.health = healthLeft;
	        
	        
	    };
	    

	    this.faint = function(){
	        console.log(this.name + ' fainted. ');
	    };

	   
	}


	let pikachu = new Pokemon("Pikachu", 12);
		console.log(pikachu);

	let geodude = new Pokemon("Geodude", 8);
		console.log(geodude);
	
	let mewtwo = new Pokemon("Mewtwo", 100);
		console.log(mewtwo);
	

	geodude.tackle(pikachu);
	console.log(pikachu);

	mewtwo.tackle(geodude);
	console.log(geodude);

